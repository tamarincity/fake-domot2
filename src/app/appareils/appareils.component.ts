import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../service/appareils-service';

@Component({
  selector: 'app-appareils',
  templateUrl: './appareils.component.html',
  styleUrls: ['./appareils.component.scss']
})
export class AppareilsComponent implements OnInit {

  appareils = [];
  constructor(private appareilService: AppareilService) { }

  ngOnInit() {
    this.appareils = this.appareilService.appareils;
  }

  onToggleStatut(appareilId) {
    this.appareils[appareilId].status = this.appareils[appareilId].status === 'éteint' ? 'allumé' : 'éteint';
  }

}
