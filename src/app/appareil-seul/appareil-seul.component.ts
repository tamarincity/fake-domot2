import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AppareilService } from '../service/appareils-service';


@Component({
  selector: 'app-appareil-seul',
  templateUrl: './appareil-seul.component.html',
  styleUrls: ['./appareil-seul.component.scss']
})
export class AppareilSeulComponent implements OnInit {
  idRecupere: any;
  appareils = [];
  constructor(private route: ActivatedRoute, private appareilService: AppareilService) { }

  ngOnInit() {
    this.idRecupere = this.route.snapshot.params['id'];
    this.appareils = this.appareilService.appareils;
    console.log(this.appareils);
  }

}
