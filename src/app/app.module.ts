import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AppareilsComponent } from './appareils/appareils.component';
import { LoginComponent } from './login/login.component';

import { AppareilService } from './service/appareils-service';
import { AppareilSeulComponent } from './appareil-seul/appareil-seul.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'appareils', component: AppareilsComponent },
  { path: 'appareils/:id', component: AppareilSeulComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    AppareilsComponent,
    LoginComponent,
    AppareilSeulComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [AppareilService],
  bootstrap: [AppComponent]
})
export class AppModule { }
